#ifndef __NEVAL_H
#define __NEVAL_H

#include <stddef.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/*************************************************************************************************/

double neval_polynomial(size_t ncoef, double p[ncoef], double x);

/*************************************************************************************************/

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __NEVAL_H */