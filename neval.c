#include "neval.h"

/*************************************************************************************************/

double neval_polynomial(size_t n, double p[n], double x)
{
	double si = *p;
	for(size_t i = n - 1; i--;)
	{
		p++;
		si = (*p) + (si * x);
	}
	return si;
}
